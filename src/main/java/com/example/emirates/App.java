package com.example.emirates;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 */
public class App implements Adder {
    private int val1, val2;

    public int add(int i, int j) {
        val1 = i;
        val2 = j;

        return i + j;
    }

    @Override
    public int numberOfPairs(int[] input, int k) {
        Map<Integer, Integer> pairs = new HashMap<Integer, Integer>();

        int count = 0;

        for (int i = 0; i < input.length; i++) {

            if (pairs.containsKey(input[i])) {
                System.out.println(input[i] + ", " + pairs.get(input[i]));
                count++;
            } else {
                pairs.put(k - input[i], input[i]);

            }
        }
        return count;
    }

   // public static void main(String[] args) {
        //String p = "<p>My paragraph</p>";
        //App ap = new App();

        //// int res = ap.add(1, 2);
        //int[] a = {1, 3, 46, 1, 3, 9};
        //int pair = ap.numberOfPairs(a, 47);

        //// System.out.println(res);
        //System.out.println("Number of distinct pair is " + pair);

        //int[] b = {6, 6, 3, 9, 3, 5, 1};
        //int pair2 = ap.numberOfPairs(b, 12);
        //System.out.println("Number of distinct pair is " + pair2);

        //// System.out.println("");

        //// System.out.println(StringEscapeUtils.escapeHtml4(p));
    //}
}
