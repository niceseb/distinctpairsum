package com.example.emirates;

/**
 * Created by mincheung on 22/03/2017.
 */
public interface Adder {

    public int numberOfPairs(int[] input, int k);
}
