package com.example.emirates;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * Unit test for simple App.
 */
public class AppTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    /**
     * Rigorous Test :-)
     */
    public void testApp() {
        assertTrue(true);
    }

    /**
     * Added
     **/
    public void testAdd() {
        App ap = new App();
        int result_add = ap.add(3, 4);
        assertEquals(result_add, 7);
    }

    public void testFindOnePair() {
        int[] a = {1, 3, 46, 1, 3, 9};
        App ap = new App();
        int pair = ap.numberOfPairs(a, 47);
        assertEquals(pair, 1);
    }

    public void testFindTwoPair() {
        int[] a = {6, 6, 3, 9, 3, 5, 1};
        App ap = new App();
        int pair = ap.numberOfPairs(a, 12);
        assertEquals(pair, 2);
    }

}
